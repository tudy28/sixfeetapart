from django.db import models
from django.core.paginator import PageNotAnInteger,Paginator,EmptyPage


from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel,PageChooserPanel,StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from streams import blocks

class HomePage(Page):
    templates = "home/home_page.html"
    max_count = 1

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["posts"] = Story.objects.live().public().order_by("last_published_at").reverse()
        size = len(context["posts"])
        stories = None
        if size > 9:
            posts = context["posts"]
            paginator = Paginator(posts[9:], 3)  # @todo change to 9
            page = request.GET.get("page")
            try:
                stories = paginator.page(page)
            except PageNotAnInteger:
                stories = paginator.page(1)
            except EmptyPage:
                stories = paginator.page(paginator.num_pages)
            context["paginated_posts"] = stories
        elif size == 4 or size == 7:
            posts = context["posts"]
            paginator = Paginator(posts[size-1:], 3)  # @todo change to 9
            page = request.GET.get("page")
            try:
                stories = paginator.page(page)
            except PageNotAnInteger:
                stories = paginator.page(1)
            except EmptyPage:
                stories = paginator.page(paginator.num_pages)
            context["paginated_posts"] = stories
        elif size == 5 or size == 8:
            posts = context["posts"]
            paginator = Paginator(posts[size - 2:], 3)  # @todo change to 9
            page = request.GET.get("page")
            try:
                stories = paginator.page(page)
            except PageNotAnInteger:
                stories = paginator.page(1)
            except EmptyPage:
                stories = paginator.page(paginator.num_pages)
            context["paginated_posts"] = stories





        return context



class Story(Page):
    templates = "stories/story_page.html"
    banner_title = RichTextField(features=["bold", "italic", "underline"])
    banner_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="+"
    )
    content = StreamField([
        ("title_and_text", blocks.TitleAndText()),
        ("rich_title_and_text",blocks.RichTitleAndText())
    ])



    content_panels = Page.content_panels + [
        FieldPanel("banner_title"),
        ImageChooserPanel("banner_image"),
        StreamFieldPanel("content"),
    ]
