from wagtail.core import blocks
from django.db import models
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel,PageChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel

class TitleAndText(blocks.StructBlock):
    """Only title and text Fields"""
    title = blocks.CharBlock(required=True, help_text='Add your title here')
    text = blocks.TextBlock(required=True, help_text="Add your text here")

    class Meta:
        template = "streams/title_and_text_block.html"
        icon = "edit"
        label = "Title & Text"

class RichTitleAndText(blocks.RichTextBlock):
    """Rich tile and text  with all the fixings"""
    class Meta:
        template = "streams/rich_title_and_text_block.html"
        icon = "edit"
        label = "Rich Title & Text"
